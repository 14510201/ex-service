# Dockerized Service Deployment

This project focuses on deploying executables or binaries as services within Docker containers. It is aimed at developers or operations teams looking to containerize their applications for consistent, scalable, and isolated execution environments.



## Getting started

These instructions will guide you through setting up and running your Dockerized services.

### Prerequisites

Before starting, ensure you have the following installed:
- Docker
- Git (optional, if cloning the project repository)

### Installation
Clone the repo
```
git clone [repository_url]

```
Prepare the Docker Environment

### Download
Download /bring  lib, bin, include directory. Also log4cpp.properties and [vg-packet-forwarder](https://atl-gitlab.ao-inc.home:8929/qlink-vlgw/vg-packet-forwarder) 

Ensure that all required executables and libraries (lib, bin, include) are in the same directory as your Dockerfile. Also log4cpp.properties and vg-packet-forwarder

### Build the Docker Image
Customize newimagename:tag with your desired image name and version.

```
docker build --no-cache -t newimagename:tag .
# Example:
docker build --no-cache -t servicecontainer:latest .

```
### Running Your Service in Docker
Deploy your service using the following command:
```
docker run -d --name your_service_container --network="host" newimagename:tag
# Example:
docker run -d --name myservice_container --network="host" servicecontainer:latest

```
To pass environment variables and customize your service's configuration, use:

```
docker run -d --name your_service_container --network="host" -e CUSTOM_VAR="value" newimagename:tag
# Example with environment variables:
docker run -d --name myservice_container --network="host" -e NDR_CLIENT_S="0x80001ff5" servicecontainer:latest

```

### Default Configuration
Below parameters is default params. 
Common Environment variables
- GW_ID=eb9a02ab45feb421
- REDIS_IP=192.168.44.67
- REDIS_PORT=6379
- SRC_IP=112.97.0.38
- INTERFACE=enp2s0

Environment variables for NDR CLIENT
- NDR_CLIENT_S=0x80001ff1
- NDR_CLIENT_DST_IP=112.97.0.47

Environment variables for NDF AGENT
- NDF_AGENT_S=0x80001ff0
- NDF_AGENT_M=239.1.2.3

Adjust these based on the services you are deploying.

### Monitoring and Logs
All Service Status
```
docker exec -it your_service_container supervisorctl status
# Example:
docker exec -it myapp_container supervisorctl status

```
Service Logs
```
docker exec your_service_container cat /path/to/service/log.log
# Replace /path/to/service/log.log with the actual log file path
# Example: 
docker exec myapp_container cat /var/log/vg-ndr-client/vg-ndr-client_stdout.log
docker exec myapp_container cat /var/log/vg-ndf-agent/vg-ndf-agent_stdout.log
docker exec myapp_container cat /var/log/vg-packet-forwarder/vg-packet-forwarder_stdout.log

```
Container Logs
```
docker logs your_service_container
# Example:
docker logs myapp_container

```

### Stopping a Docker Container
To stop the running container:
```
docker stop your_service_container
# Example:
docker stop myapp_container
```

### Removing a Docker Container
Once the container has been stopped, you can remove it:
```
docker rm your_service_container
# Example:
docker rm myapp_container
```

